# DataEnricher

## Overview

Restructure data formats and models on the fly by using a drag and drop interface with no need for an extra ETL tool in your IoT project. Security and ACL are implemented by default while saving 2G, 3G and 4G bandwidth with a marketplace of full standard data models. Loop Data Enricher is integral to aggregating and normalizing data coming from many disparate systems.

## Getting Started

### STEP 1:

Data-Enricher can be synchronized from litmus.pro by navigating to Applications, and then clicking the synchronize button as showm

![alt text](https://bytebucket.org/litmusloopdocs/dataenricher/raw/master/synchronize.png)

### STEP 2:

Data Enricher shows the list of projects as soon as you log into it. The List is exactly similar to the list of projects you see under your company. 

![alt text](https://bytebucket.org/litmusloopdocs/dataenricher/raw/master/project_list.png)

New project can be created manually by clicking the *NEW* tab, and providing the necessary details

![alt text](https://bytebucket.org/litmusloopdocs/dataenricher/raw/master/Create_project_tab.png)

Once saved you can see the list of projects on the first page. Click on the particular project you want to access.

### STEP 3:

Once you enter a particular project, you see the complete dashboard of data enricher. It contains the list of topics which needs to be entered by the user.

![alt text](https://bytebucket.org/litmusloopdocs/dataenricher/raw/master/Data_Enricher_dashboard.png)

Click on *NEW* to enter the topic on which data is being published currently.

![alt text](https://bytebucket.org/litmusloopdocs/dataenricher/raw/master/New_topic.png)

Click on the *Start grabbing input topic* icon to the right and it will fetch the current data being publised to the topic entered. 

![alt text](https://bytebucket.org/litmusloopdocs/dataenricher/raw/master/live_data.png)

Click Save to save the topic once the topic is defined and live data is fetched.


### Step 4:

Click on *Start topic builder* icon on the top-right corner of the page, or click on the number of Steps to enter to customize the data on the fly

![alt text](https://bytebucket.org/litmusloopdocs/dataenricher/raw/master/topic_status.png)

Select the *Template* from the drop down list. Enter the output topic you want the enriched data to be published to.

**NOTE** The user sees the templates which are present in the My templates tab, in the dropdown list.

![alt text](https://bytebucket.org/litmusloopdocs/dataenricher/raw/master/accelerometer.png)

You will see the current payload on the left and the standard template on the right. Drag the data points from the current payload to match the data points on the Standard template on the right.

![alt text](https://bytebucket.org/litmusloopdocs/dataenricher/raw/master/data_point_connected.png)

In the current example shown above the device is sending accelerometer data on the topic **demoroom/linkit/allpub**. The input payload does not contain the meta data or any other details. We can connect the x,y,z values of the accelerometer to the "value x","value y","value z" of the stanadard template.Once connected click *RUN* and you can see the enriched data on the right tab. The enriched data is being published to the Output topic provided by the user **demoroom/accelerometer**.

![alt text](https://bytebucket.org/litmusloopdocs/dataenricher/raw/master/enriched_data.png)

### Settings

The *Settings* tab contains the peoject details which are synchronized automatically from litmus.pro

*General*

The general tab consists of the project name and the project description. It can be edited if needed and description upto 150 words can be added.

![alt text](https://bytebucket.org/litmusloopdocs/dataenricher/raw/master/Settings/settings_general.png)

*Broker*

Broker consists of two tabs **Connection** and **Security**. 

Connection consists of the server details that is the Server, Port, CleintId. The parameters are synchronized autmatically and user only needs to enter it manually if they create a standalone project inside Data Enricher.

![alt text](https://bytebucket.org/litmusloopdocs/dataenricher/raw/master/Settings/Settings_Broker_connection.png)

Security consists of Username and passowrd for the connection. It is also synchronized automatically and user needs to be enter it manually if they create a standaloone project inside Data Enricher. The SSL if used can also be enabled and verified under security tab.

![alt text](https://bytebucket.org/litmusloopdocs/dataenricher/raw/master/Settings/Settings_Broker_Security.png)

*User*

Users can be added to the Data enricher project from this tab. If there are existing users on the project one can define their role as follows:
**Guest**- Can read only
**Master**- Can modify project attributes and topics
**Owner**- Can delete a project and assign project members

![alt text](https://bytebucket.org/litmusloopdocs/dataenricher/raw/master/Settings/Setting_Users.png)

*Delete*

Projects can be deleted under Settings-> Delete section

![alt text](https://bytebucket.org/litmusloopdocs/dataenricher/raw/master/Settings/Setting_Delete.png)

### Templates

Templates consists of two sections, My templates and public templates. 

The **Public templates** consists of standard templates as per OMA standards. Click on the **Copy to My Template Library* button under Action to copy it to My templates.

![alt text](https://bytebucket.org/litmusloopdocs/dataenricher/raw/master/Templates/public_template.png)

The **My templates** consists of the templates which are downloaded from Public templates and also the templates designed by the user. 
User can create thier own template by clicking the *NEW* tab and adding the details.

![alt text](https://bytebucket.org/litmusloopdocs/dataenricher/raw/master/Templates/Template_edit.png)

The Advanced tab in the Template Edit tab can be used to define the tags of the device. Once added click save to see the templates in My templates

![alt text](https://bytebucket.org/litmusloopdocs/dataenricher/raw/master/Templates/my_template_added.png)